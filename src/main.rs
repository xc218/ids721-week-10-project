use warp::Filter;

#[tokio::main]
async fn main() {
    // Define API routes
    let hello = warp::path!("hello" / String)
        .map(|name| format!("Hello, {}", name));

    // Start the server
    warp::serve(hello)
        .run(([0, 0, 0, 0], 3030))
        .await;
}
