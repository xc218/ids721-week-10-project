# Ids721 Week 10 Project

### Requirements
Dockerize Hugging Face Rust transformer
Deploy container to AWS Lambda
Implement query endpoint
### Grading Criteria
Transformer packaging: 30%
Serverless deployment: 30%
Endpoint functionality: 30%
Documentation: 10%
### Deliverables
Dockerfile and Rust code
Screenshot of AWS Lambda
cURL request against endpoint

### Steps

#### Develop Rust Application
1. Create a New Rust Project
```
cargo new rust_transformer
cd rust_transformer
```
2. Add Dependencies to `Cargo.toml`
```
[dependencies]
tokio = { version = "1", features = ["full"] }
warp = "0.3"
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
```
3. Implement Application Logic
This simple application starts a server that responds with "Hello, [name]" when visited.
```
use warp::Filter;

#[tokio::main]
async fn main() {
    // Define API routes
    let hello = warp::path!("hello" / String)
        .map(|name| format!("Hello, {}", name));

    // Start the server
    warp::serve(hello)
        .run(([0, 0, 0, 0], 3030))
        .await;
}

```
4. Build with `Cargo build`
#### Dockerize the Application
1. Create a Dockerfile
```
FROM rust:latest as builder

WORKDIR /usr/src/myapp

# Copy the manifest
COPY ./Cargo.toml ./Cargo.lock ./

# This dummy build is to cache your dependencies; it will be thrown away
RUN mkdir src && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/myapp*

# Now copy your actual source code
COPY ./src ./src

# Build your application
RUN cargo build --release

```

2. Build and Test the Docker Image

Build the Docker Image:
`docker build -t rust_transformer .`

Run the container locally to test:
`docker run -p 3030:3030 rust_transformer`

#### Deploy to AWS Lambda
1. Create ECR Repository:
`aws ecr create-repository --repository-name rust_transformer`
2. Authenticate Docker to ECR:
`aws ecr get-login-password --region your-region | docker login --username AWS --password-stdin your-aws-account-id.dkr.ecr.your-region.amazonaws.com`
3. Tag and Push Docker Image
`docker tag rust_transformer:latest your-aws-account-id.dkr.ecr.your-region.amazonaws.com/rust_transformer:latest`
`docker push your-aws-account-id.dkr.ecr.your-region.amazonaws.com/rust_transformer:latest`
4. Create Lambda Function with the Container Image
Go to the AWS Lambda console, create a new function, choose "Container image", and select the image you pushed.
5. Set Up API Gateway
* Create a new API in API Gateway.
* Configure a new resource and method to invoke your Lambda function.
* Deploy the API.

![screenshot](screenshot.png)

6. Using cURL request
`curl -X GET 'https://your-api-id.execute-api.your-region.amazonaws.com/your-stage/hello/World'`