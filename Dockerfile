FROM rust:latest as builder

WORKDIR /usr/src/myapp

# Copy the manifest
COPY ./Cargo.toml ./Cargo.lock ./

# This dummy build is to cache your dependencies; it will be thrown away
RUN mkdir src && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/myapp*

# Now copy your actual source code
COPY ./src ./src

# Build your application
RUN cargo build --release
